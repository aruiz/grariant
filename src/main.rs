use std::mem;
use std::mem::transmute;

trait Addressable {}
impl Addressable for u8 {}
impl Addressable for u16 {}
impl Addressable for u32 {}
impl Addressable for u64 {}

pub struct GVariant<'a> {
  format: String,
  buffer: &'a [u8],
  astroot: Option<Box<VariantNode>>,
}

struct VariantNode {
  vtype: VariantType,
  children: Vec<Box<VariantNode>>,
}

impl VariantNode {
  fn new (vtype: VariantType) -> VariantNode {
    VariantNode {vtype: vtype, children: vec![]}
  }

  fn is_fixed_size (&self) -> bool {
    match self.get_size() { None => { false } _ => { true }}
  }

  fn is_container (&self) -> bool {
    match self.vtype {
      VariantType::Tuple | VariantType::Maybe | VariantType::Array | VariantType::Dict => {
        true
      }
      _ => { false }
    }
  }

  fn get_size (&self) -> Option<usize> {
    match self.vtype {
      VariantType::Array | VariantType::Maybe | VariantType::V | VariantType::S | VariantType:: O | VariantType::G => {
        return None;
      }
      VariantType::Tuple | VariantType::Dict => {
        let children = &self.children;
        children.into_iter().map(|x| x.get_size()).fold(Some(0), |a, b| -> Option<usize> {
          match a {
            None => { return None }
            _ => { ; }
          }
          match b {
            None => { return None }
            _ => { ; }
          }
          Some(a.unwrap() + b.unwrap())
        })
      }
      _ => { self.vtype.get_size() }
    }
  }

  fn min_size (&self) -> usize {
    match self.vtype {
      VariantType::Array | VariantType::Maybe | VariantType::V => {
        return 0;
      }
      VariantType::Tuple | VariantType::Dict => {
        {&self.children}.into_iter().map(|x| x.min_size()).fold(0, |a, b| { a + b })
      }
      VariantType::S | VariantType:: O | VariantType::G => { 0 }
      ref e @ _ => { match e.get_size() { Some(e) => e, None => {panic!("Should not reach");} } }
    }
  }

  fn to_format_string (&self) -> String {
    let children = &self.children;
    match self.vtype {
      VariantType::Array => {
        let mut gvtype = String::from("a");
        gvtype.push_str(children[0].to_format_string ().as_str());
        gvtype
      }
      VariantType::Maybe => {
        let mut gvtype = String::from("m");
        gvtype.push_str(children[0].to_format_string ().as_str());
        gvtype
      }
      VariantType::Tuple => {
        let mut gvtype = String::from("(");
        for c in children.into_iter() {
          gvtype.push_str(c.to_format_string().as_str());
        }
        gvtype.push(')');
        gvtype
      }
      VariantType::Dict => {
        let mut gvtype = String::from("{");
        gvtype.push_str(children[0].to_format_string ().as_str());
        gvtype.push_str(children[1].to_format_string ().as_str());
        gvtype.push('}');
        gvtype
      }
      _ => {
        let mut gvtype = String::new();
        gvtype.push(self.vtype.to_char());
        gvtype
      }
    }
  }
  fn from_format (format: &String) -> Option<Box<VariantNode>> {
    let mut offset: usize = 1;
    let mut stack: Vec<Box<VariantNode>> = vec![Box::new(VariantNode::new(VariantType::Root))];

    for c in format.chars() {
      let pop_container = |stack: &mut Vec<Box<VariantNode>>| {
        let container = stack.pop().unwrap();
        stack.last_mut().unwrap().as_mut().children.push(container);
      };
      match c {
        'b'|'y'|'n'|'q'|'i'|'u'|'x'|'t'|'h'|'d'|'s'|'o'|'g'|'v' => {
          stack.last_mut().unwrap().as_mut ().children.push(Box::new(VariantNode::new(VariantType::char_to_vtype(c))));
        }
        'a'|'m'|'('|'{' => {
          stack.push(Box::new(VariantNode::new(VariantType::char_to_vtype(c))));
        }
        ')' => {
          match stack.last().unwrap().vtype {
            VariantType::Tuple => { ; }
            _ => { panic!("Invalid syntax, closing a not yet open tuple with {} in column {} of variant format string '{}'", c, offset, format); }
          }

          pop_container (&mut stack);
        }
        '}' => {
          match stack.last().unwrap().vtype {
            VariantType::Dict => { ; }
            _ => { panic!("Invalid syntax, closing a not yet open dictionary with {} in column {} of variant format string '{}'", c, offset, format); }
          }

          if stack.last().unwrap().children.len() != 2 {
            panic!("Dictionary in column {} of variant format string '{}' does not have two items", offset - stack.last().unwrap().children.len(), format);
          }

          pop_container (&mut stack);
        }
         _ => { panic!("Item {} in {} is not a valid GVariant string character", c, format); }
      }


      match stack.last().unwrap().vtype {
        VariantType::Array | VariantType::Maybe => {
          while stack.last().unwrap().vtype == VariantType::Array ||
                stack.last().unwrap().vtype == VariantType::Maybe {
            match stack.last().unwrap().children.len() {
              0 => { break; }
              1 => { pop_container(&mut stack); }
              _ => { ; }
            }
          }
        }
        VariantType::Dict => {
          if stack.last().unwrap().children.len() > 2 {
              panic!("Dictionaries can only have a key/value pair of items: {}", format);
          }
        }
        VariantType::Root => {
          if stack.last().unwrap().children.len() > 1 {
              panic!("GVariant strings cannot have more than one root element, try using a tuple () instead: {}", format);
          }
        }
        _ => { ; }
      }

      offset += 1;
    }

    match stack.last().unwrap().vtype {
      VariantType::Root  => { ; }
      VariantType::Tuple => { panic!("A tuple was left open in column {} of variant string '{}'", offset, format); }
      VariantType::Dict  => { panic!("A dict was left open in column {} of variant string '{}'", offset, format); }
      VariantType::Array => { panic!("An array was specified without a type in column {} of variant string '{}'", offset, format); }
      VariantType::Maybe => { panic!("A maybe type was specified without a type in column {} of variant string '{}'", offset, format); }
      _ => { panic!("Should not reach this case, variant format parser stack did not end on Root {}", format); }
    }

    /* Skip the Root node */
    Some(stack.pop().unwrap().children.pop().unwrap())
  }

  fn get_min_size (&self) -> usize {
    match &self.vtype {
      &VariantType::Tuple | &VariantType::Dict | &VariantType::S | &VariantType::O | &VariantType::G => {
        let mut final_size = 0;
        for i in self.children.iter() {
          final_size = final_size + i.get_min_size()
        }
        final_size
      }
      &VariantType::Array | &VariantType::Maybe | &VariantType::V => {
        0
      }
      &VariantType::Root => {
        panic!("Can't get size for Root type");
      }
      e @ _ => {
        e.get_size().unwrap()
      }
    }
  }
}

impl Clone for VariantNode {
  fn clone (&self) -> VariantNode {
    VariantNode { vtype: self.vtype.clone(), children: self.children.clone() }
  }

  fn clone_from (&mut self, from: &VariantNode) {
    self.vtype = from.vtype.clone();
    self.children = from.children.clone();
  }
}

#[derive(Debug, PartialEq, Clone)]
enum VariantType {
    /* leaf types */
    B, Y, N, Q, I, U, X, T, H, D, S, O, G, V,
    /* container type */
    Array,
    Maybe,
    Tuple,
    Dict,
    Root,
}

impl VariantType {
  fn char_to_vtype (c: char) -> VariantType {
    match c {
      'b' => { VariantType::B },
      'y' => { VariantType::Y },
      'n' => { VariantType::N },
      'q' => { VariantType::Q },
      'i' => { VariantType::I },
      'u' => { VariantType::U },
      'x' => { VariantType::X },
      't' => { VariantType::T },
      'h' => { VariantType::H },
      'd' => { VariantType::D },
      's' => { VariantType::S },
      'o' => { VariantType::O },
      'g' => { VariantType::G },
      'v' => { VariantType::V },
      'a' => { VariantType::Array },
      'm' => { VariantType::Maybe },
      '(' => { VariantType::Tuple },
      '{' => { VariantType::Dict },
      _ => { panic!("char {} not supported", c)}
    }
  }

  fn to_char(&self) -> char {
    match *self {
      VariantType::B => {'b'},
      VariantType::Y => {'y'},
      VariantType::N => {'n'},
      VariantType::Q => {'q'},
      VariantType::I => {'i'},
      VariantType::U => {'u'},
      VariantType::X => {'x'},
      VariantType::T => {'t'},
      VariantType::H => {'h'},
      VariantType::D => {'d'},
      VariantType::S => {'s'},
      VariantType::O => {'o'},
      VariantType::G => {'g'},
      VariantType::V => {'v'},
      VariantType::Array => {'s'},
      VariantType::Maybe => {'m'},
      VariantType::Tuple => {'('},
      VariantType::Dict => {'{'},
      VariantType::Root => { panic!("Root is an internal type and does not have a variant format character") }
    }
  }

  fn get_size (&self) -> Option<usize> {
    match *self {
      VariantType::B | VariantType::Y => { Some(1) }
      VariantType::N | VariantType::Q => { Some(2) }
      VariantType::I | VariantType::U | VariantType::H => { Some(4) }
      VariantType::X | VariantType::T | VariantType::D => { Some(8) }
      _ => { return None }
    }
  }
}

trait Unmarshall<'a> {
    fn from_gvariant (buffer: &'a [u8], index: usize) -> Self;
}

fn generic_unmarshall_leaf_type<T> (buffer: &[u8], index: usize) -> *const T {
  let len = buffer.len();
  let typesize = mem::size_of::<T>();
  if index + 1 > len || index + typesize > len {
    panic!("Trying to unmarshall value out of GVariant buffer scope");
  }
  unsafe {
    buffer.as_ptr ().offset(index as isize) as *const T
  }
}

macro_rules! unmarshaller_for_type {
  ($e:ty) => {
    impl<'a> Unmarshall<'a> for $e {
      fn from_gvariant (buffer: &'a [u8], index: usize) -> $e {
         unsafe {
          return *generic_unmarshall_leaf_type::<$e> (buffer, index);
         }
      }
    }
  }
}

unmarshaller_for_type!(bool);
unmarshaller_for_type!(u8);
unmarshaller_for_type!(i16);
unmarshaller_for_type!(u16);
unmarshaller_for_type!(i32);
unmarshaller_for_type!(u32);
unmarshaller_for_type!(i64);
unmarshaller_for_type!(u64);
unmarshaller_for_type!(f64);

impl<'a> Unmarshall<'a> for &'a str {
  fn from_gvariant (buffer: &'a [u8], index: usize) -> &'a str {
    for i in (index)..(buffer.len()) {
      if buffer[i] == 0 {
        match std::str::from_utf8(&buffer[index..i]) {
          Err(e) => { panic!("GVariant with invalid UTF8 data: {}", e); },
          Ok(v) => { return v; }
        }
      }
    }
    panic!("Tried to extract string but no null character was found in the buffer");
  }
}

impl<'a> Unmarshall<'a> for GVariant<'a> {
  fn from_gvariant (buffer: &'a [u8], index: usize) -> GVariant<'a> {
    let bound_buffer = &buffer[index..buffer.len()];

    for (i, c) in bound_buffer.iter().enumerate().rev() {
      if *c != 0 { continue; }

      let fmt = std::str::from_utf8 (&bound_buffer[i+1..bound_buffer.len()]).unwrap();
      return GVariant::new (fmt, &bound_buffer[0..i]);
    }
    panic!("No null character in variant element data, could not find format string");
  }
}

trait GetValue<T> {
  fn get (&self) -> T;
}

macro_rules! value_getter_for_type {
  ($t:ty, $($m:expr),+) => {
    impl<'a> GetValue<$t> for GVariant<'a> {
      fn get (&self) -> $t {
        match self.format.as_str() {
          $($m)|+  => { <$t>::from_gvariant (self.buffer, 0) }
          _ => { panic!("Wrong type: {}", self.format); }
        }
      }
    }
  }
}

value_getter_for_type!(u8, "b", "y");
value_getter_for_type!(bool, "b");
value_getter_for_type!(i16, "n");
value_getter_for_type!(u16, "q");
value_getter_for_type!(i32, "i", "h");
value_getter_for_type!(u32, "u");
value_getter_for_type!(i64, "x");
value_getter_for_type!(u64, "t");
value_getter_for_type!(f64, "d");
value_getter_for_type!(&'a str, "s", "o", "g");
value_getter_for_type!(GVariant<'a>, "v");

impl<'a, T> GetValue<Option<T>> for GVariant<'a>
  where GVariant<'a>: GetValue<T>, {
  fn get (&self) -> Option<T> {
    match self.astroot {
      Some(ref root) => {
        match root.vtype {
          VariantType::Maybe => {
            if self.buffer.len() == 0 {
              return None;
            }

            let buffer: &[u8];
            match root.children[0].vtype.get_size() {
              Some(_) => { buffer = self.buffer; }
              None => { buffer = &self.buffer[..self.buffer.len()-1]; }
            }
            let child = GVariant { astroot: Some(root.children[0].clone()),
                                   format: String::from(&self.format.as_str()[1..]),
                                   buffer: buffer };
            return Some (child.get());

          }
          _ => { panic!("Tried to get a Maybe type from a non Maybe variant {}", self.format); }
        }
      }
      _ => { panic!("This GVariant structure was not constructed with new"); }
    }
  }
}

trait GetChildren<'a, T> {
  fn get_child(&'a self, index: usize) -> T;
}

fn get_value_slice_for_size<'a, T> (buffer: &'a [u8], index: usize) -> &'a [u8]
  where T: Unmarshall<'a> + PartialEq {
  let offset_type_size = mem::size_of::<T> ();
  let array_end = match offset_type_size {
    1 => { <u8>::from_gvariant(buffer, buffer.len() - offset_type_size) as usize }
    2 => { <u16>::from_gvariant(buffer, buffer.len() - offset_type_size) as usize }
    4 => { <u32>::from_gvariant(buffer, buffer.len() - offset_type_size) as usize }
    8 => { <u64>::from_gvariant(buffer, buffer.len() - offset_type_size) as usize }
    _ => { panic!("pointer size bigger than 64 bit not supported"); }
  };

  if array_end == buffer.len() - offset_type_size {
    if index > 0 {
      panic!("Array value out of index");
    }
    return &buffer[0..buffer.len() - offset_type_size]
  } else if array_end > buffer.len() - 1 {
    panic!("GVariant data is corrupted or malformed, array offset value out of index");
  }

  let offset_array_length = buffer.len() - array_end;

  if offset_array_length % offset_type_size != 0 {
    panic!("Array of data offsets for array has wrong size, is not a multiple of the size of the offset type");
  }

  let n_elements = offset_array_length / offset_type_size;
  if index >= n_elements {
    panic!("Tried to find array element on index {} but array is of length {}", index, n_elements);
  }

  match index {
    0 => {
      let lower_index = buffer.len() - (n_elements - index) * offset_type_size;
      let lower_bound = match offset_type_size {
        1 => { <u8>::from_gvariant(buffer, lower_index) as usize }
        2 => { <u16>::from_gvariant(buffer, lower_index) as usize }
        4 => { <u32>::from_gvariant(buffer, lower_index) as usize }
        8 => { <u64>::from_gvariant(buffer, lower_index) as usize }
        _ => { panic!("pointer size bigger than 64 bit not supported"); }
      };
      &buffer[0..lower_bound]
    }
    _ => {
      let (upper_index, lower_index) = (array_end + (index-1) * offset_type_size, array_end + index * offset_type_size);
      let (upper_bound, lower_bound) = match offset_type_size {
        1 => { (<u8>::from_gvariant(buffer, upper_index) as usize, <u8>::from_gvariant(buffer, lower_index) as usize) }
        2 => { (<u16>::from_gvariant(buffer, upper_index) as usize, <u16>::from_gvariant(buffer, lower_index) as usize) }
        4 => { (<u32>::from_gvariant(buffer, upper_index) as usize, <u32>::from_gvariant(buffer, lower_index) as usize) }
        8 => { (<u64>::from_gvariant(buffer, upper_index) as usize, <u64>::from_gvariant(buffer, lower_index) as usize) }
        _ => { panic!("pointer size bigger than 64 bit not supported"); }
      };
      &buffer[upper_bound..lower_bound]
    }
  }
}

fn get_child_slice_from_uneven_array<'a> (buffer: &'a [u8], index: usize) -> &'a [u8] {
  if buffer.len() < <u8>::max_value() as usize {
    return get_value_slice_for_size::<u8> (buffer, index);
  }
  if buffer.len() < <u16>::max_value() as usize {
    return get_value_slice_for_size::<u16> (buffer, index);
  }
  if buffer.len() < <u32>::max_value() as usize {
    return get_value_slice_for_size::<u32> (buffer, index);
  }
  if buffer.len() < <u64>::max_value() as usize {
    return get_value_slice_for_size::<u64> (buffer, index);
  }
  panic!("Buffer size {} not supported for array container", buffer.len());
}


fn get_child_slice_from_even_array (buffer: &[u8], index: usize, element_size: usize) -> &[u8] {
  if buffer.len() % element_size != 0 {
    panic!("Buffer size is not a multiple of type size");
  }

  let n_items = buffer.len() / element_size;

  if index >= n_items {
    panic!("Index out of range trying to get a value out of an array");
  }

  &buffer[index*element_size..(index + 1)*element_size]
}

fn get_child_slice_from_tuple<'a> (variant: &'a GVariant, index: usize) -> &'a [u8] {
  fn get_offset (buffer: &[u8], item_size: usize, index: usize) -> usize {
    match item_size {
      1 => { <u8>::from_gvariant(buffer, index) as usize }
      2 => { <u16>::from_gvariant(buffer, index) as usize }
      4 => { <u32>::from_gvariant(buffer, index) as usize }
      8 => { <u64>::from_gvariant(buffer, index) as usize }
      _ => { panic!(""); }
    }
  }

  fn get_offset_array<'a> (buffer: &'a [u8], n_items: usize) -> (&'a [u8], usize) {
    let item_size: usize;
    let len = buffer.len();

    if len < <u8>::max_value() as usize       { item_size = mem::size_of::<u8>(); }
    else if len < <u16>::max_value() as usize { item_size = mem::size_of::<u16>(); }
    else if len < <u32>::max_value() as usize { item_size = mem::size_of::<u32>(); }
    else if len < <u64>::max_value() as usize { item_size = mem::size_of::<u64>(); }
    else { panic!("The buffer is too big to be addressed by a 64bit integer"); }

    let offset_size = item_size * n_items;
    if offset_size > buffer.len() {
      panic!("buffer of size {} can't possibly hold {} values of size {}", buffer.len(), n_items, item_size);
    }

    (&buffer[buffer.len() - offset_size .. buffer.len()], item_size)
  }

  fn count_offsets (node: &VariantNode) -> usize {
    let mut n_offsets = 0usize;
    for i in 0..node.children.len() {
      match node.children[i].get_size() {
        Some(_) => { ; }
        None => {
          if i != node.children.len()-1 {
            n_offsets += 1;
          }
        }
      }
    }
    n_offsets
  }

  fn find_slice_for_child<'a> (buffer: &'a [u8],
                               node: &VariantNode,
                               index: usize) -> &'a [u8] {
    let n_offsets = count_offsets(node);
    let (offsets, offset_size) = get_offset_array (buffer, n_offsets);
    if index >= node.children.len() { panic!("index out of range"); }
    if node.min_size()+n_offsets > buffer.len() { panic!("buffer of size {} is smaller than minimal tuple size '{}'", buffer.len(), node.to_format_string()); }

    let mut offset = 0 as usize;
    let mut unsized_count = 0 as usize;
    for i in 0..node.children.len() {
      match node.children[i].vtype.get_size() {
        Some(e) => {
          if i == index { return &buffer[offset..offset+e]; }
          offset += e;
        }
        None => {
          if i == node.children.len()-1 {
            return &buffer[offset..buffer.len()-offset_size];
          }
          let prev_offset = offset;
          offset = get_offset (offsets, offset_size, offsets.len() - 1 - unsized_count);
          if i == index { return &buffer[prev_offset..offset]; }
          unsized_count += 1;
        }
      }
    }
    panic!("should not reach");
  }

  match variant.astroot {
    Some(ref c) => {
      if index >= c.children.len() { panic!("out of index: {} {} {}", index, c.children.len(), c.to_format_string()); };
      match c.vtype {
        VariantType::Tuple => {
          return find_slice_for_child (variant.buffer, c, index);
        }
        _ => { panic!("Tried to extract element on a non tuple type"); }
      }
    }
    _ => { panic!("Format string not initialized"); }
  }
}

macro_rules! children_getter_for_type {
  ($t:ty, $($m:expr),+) => {
    impl<'a> GetChildren<'a, $t> for GVariant<'a> {
      fn get_child(&'a self, index: usize) -> $t {
        match self.astroot {
          Some(ref c) => {
            match c.vtype {
              VariantType::Array | VariantType::Maybe => {
                match c.children[0].vtype.to_char() {$($m)|+ => { ; }
                  _ => { panic!("Tried to extract a value from an array of incompatible type: {}", self.format); }
                }
                if c.vtype == VariantType::Maybe && index != 0 {
                  panic!("Index out of range, maybe types don't have more than one item");
                }
                match c.children[0].vtype.get_size() {
                  Some(s)  => { <$t>::from_gvariant(get_child_slice_from_even_array(self.buffer, index, s), 0) }
                  None  => { <$t>::from_gvariant(get_child_slice_from_uneven_array(self.buffer, index), 0) }
                }
              }
              ref e @ VariantType::Tuple | ref e @ VariantType::Dict => {
                if e == &VariantType::Dict && c.children.len() != 2 {
                  panic!("Dictionary entry in format strin '{}' must have two items, has {}", self.format, c.children.len());
                }
                <$t>::from_gvariant(get_child_slice_from_tuple (self, index), 0)
              }
              _ => { panic!("Variant must be a container to get children: {}", self.format); }
            }
          }
          None => { panic!("There is no data in this GVariant"); }
        }
      }
    }
  }
}

children_getter_for_type!(bool, 'b');
children_getter_for_type!(u8, 'b', 'y');
children_getter_for_type!(i16, 'n');
children_getter_for_type!(u16, 'q');
children_getter_for_type!(i32, 'i', 'h');
children_getter_for_type!(u32, 'u');
children_getter_for_type!(i64, 'x');
children_getter_for_type!(u64, 't');
children_getter_for_type!(f64, 'd');
children_getter_for_type!(&'a str, 's', 'o', 'g');

/*
impl<'a, T> GetChildren<Option<T>> for GVariant<'a>
  where T: Unmarshall<'a> {
  fn get_child(&self, index: usize) -> Option<T> {
    match self.astroot {
      Some(ref c) => {
        match c.vtype {
          VariantType::Array | VariantType::Maybe => {
            if self.buffer.len() == 0 {
              return None;
            }
            match c.children[0].get_size()) {
              Some(s) => { return Some(<T>::from_gvariant(get_child_slice_from_even_array(self.buffer, index, s), 0)); }
              None    => {
                if buffer.len() == 1 {
                  return None
                }
              }
              _ => { panic!("asd"); }
            }
          }
          _ => { panic!("Variant must be a container to get children: {}", self.format); }
        }
      }
      None => { panic!(""); }
    }
  }
}
*/

impl<'a> GetChildren<'a, GVariant<'a>> for GVariant<'a> {
  fn get_child(&'a self, index: usize) -> GVariant<'a> {
    match self.astroot {
      Some(ref c) => {
        let format = c.children[0].to_format_string();
        let buffer: &[u8];

        match c.vtype {
          VariantType::Array => {
            match (&c.children[0].vtype, c.children[0].get_size()) {
              (&VariantType::V, _)    => { return <GVariant>::from_gvariant(get_child_slice_from_uneven_array(self.buffer, index), 0); }
              (_, Some(s)) => { buffer = get_child_slice_from_even_array(self.buffer, index, s); }
              (_, None)    => { buffer = get_child_slice_from_uneven_array(self.buffer, index); }
            }
          }
          VariantType::Maybe => {
            if self.buffer.len () == 0 {
              panic!("Trying to get a child from a Maybe type that is empty");
            }
            match c.children[0].min_size() {
              0 => { buffer = &self.buffer[..self.buffer.len() - 1]; }
              _ => { buffer = &self.buffer[..self.buffer.len()]; }
            }
          }
          ref e @ VariantType::Tuple | ref e @ VariantType::Dict => {
            if e == &VariantType::Dict && c.children.len() != 2 {
              panic!("Dictionary entry in format strin '{}' must have two items, has {}", self.format, c.children.len());
            }
            buffer = get_child_slice_from_tuple (self, index);
          }
          _ => { panic!("Variant must be a container to get children: {}", self.format); }
        }


        GVariant { astroot: VariantNode::from_format(&format), format: format, buffer: buffer }
      }
      None => { panic!("There is no data in this GVariant"); }
    }
  }
}


impl<'a> GVariant<'a> {
    pub fn new (format: &str, buffer: &'a [u8]) -> Self {
        let mut variant = GVariant {format: String::from(format), buffer: buffer, astroot: None};
        variant.astroot = VariantNode::from_format (&variant.format);
        return variant;
    }
}

fn main () {
}

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! gvariant_format_test {
      ($id:ident, $(#[$m:meta]),* , $($v:expr),+) => {
        #[test]
        $(#[$m])
        *
        fn $id () {
          $(GVariant::new ($v, &[]);)
          *
        }
      }
    }

    /* Syntax errors */
    gvariant_format_test!(open_array, #[should_panic], "a", "aaaa");
    gvariant_format_test!(open_maybe, #[should_panic], "m", "mmmm");
    gvariant_format_test!(open_dict, #[should_panic], "{");
    gvariant_format_test!(open_tuple, #[should_panic], "(");
    gvariant_format_test!(close_unopen_dict, #[should_panic], "}");
    gvariant_format_test!(close_unopen_tuple, #[should_panic], ")");
    gvariant_format_test!(no_item_dict, #[should_panic], "{}");
    gvariant_format_test!(single_item_dict, #[should_panic], "{s}");
    gvariant_format_test!(three_item_dict, #[should_panic], "{sss}");

    gvariant_format_test!(leaf_types, ,"b","s","v");
    gvariant_format_test!(array_maybe, ,"ab", "mb", "aab", "mmb", "aaaaaaaaaaaaaaaaaaaaaab", "mmmmmmmmmmmmmmmmmmmmmmb", "amamamamamamamamamamamb", "mamamamamamamamamamamab", "a(ab)", "a{bb}", "aaaaa(aaaab{bb})");
    gvariant_format_test!(tuples, , "()", "(b)", "(bb)", "({bb})", "(((((a(bbb(bba())))))))");
    gvariant_format_test!(dicts, , "{ss}", "{{ss}{ss}}", "{(ss)(ss)}", "{aasaas}");

    /* Unmarshall */
    #[test]
    fn unmarshall_bool () {
      unsafe {
        let t = &mem::transmute::<bool,[u8; 1]> (true);
        let f = &mem::transmute::<bool,[u8; 1]> (false);

        let unmf = bool::from_gvariant (f, 0);
        let unmt = bool::from_gvariant (t, 0);

        assert!(unmt);
        assert!(!unmf);
      }
    }

    macro_rules! unmarshall_type {
      ($id:ident, $t:ty, $size:expr, $v:expr) => {
        #[test]
        fn $id () {
          unsafe {
            let t = &mem::transmute::<$t,[u8; $size]> ($v);
            let unmt = <$t>::from_gvariant (t, 0);
            assert_eq!(unmt, $v);
          }
        }
      }
    }

    unmarshall_type!(unmarshall_u8, u8, 1, 45);
    unmarshall_type!(unmarshall_u16, u16, 2, 60000);
    unmarshall_type!(unmarshall_i16, i16, 2, -25000);
    unmarshall_type!(unmarshall_u32, u32, 4, <u32>::max_value());
    unmarshall_type!(unmarshall_i32, i32, 4, <i32>::min_value());
    unmarshall_type!(unmarshall_u64, u64, 8, <u64>::max_value());
    unmarshall_type!(unmarshall_i64, i64, 8, <i64>::min_value());
    unmarshall_type!(unmarshall_f64, f64, 8, 1.45);

    #[test]
    fn unmarshall_str () {
      // "aaa" "bbb" "ccc"
      let strs: &[u8; 12] = &[97,97,97,0,  98,98,98,0, 99,99,99,0] ;
      let unms_a = <&str>::from_gvariant (strs, 0);
      let unms_b = <&str>::from_gvariant (strs, 4);
      let unms_c = <&str>::from_gvariant (strs, 8);

      assert_eq!("aaa", unms_a);
      assert_eq!("bbb", unms_b);
      assert_eq!("ccc", unms_c);
    }

    #[test]
    fn unmarshall_gvariant () {
      let gv: &[u8] =  &[0x61, 0x0, 0x0, 0x73];
      let unms_gv = <GVariant>::from_gvariant (gv, 0);
      assert_eq!("s", unms_gv.format.as_str());

      assert!(unms_gv.buffer.len() == 2);
      assert!(unms_gv.buffer[0] == 0x61);
      assert!(unms_gv.buffer[1] == 0x0);
    }

    #[test]
    #[should_panic]
    fn unmarshall_index_out_of_scope () {
      let t: &[u8] = &[1];
      let _unmt = u8::from_gvariant (t, 3);
    }

    #[test]
    #[should_panic]
    fn unmarshall_length_out_of_scope () {
      let t: &[u8] = &[1];
      let _unmt = f64::from_gvariant (t, 0);
    }

    #[test]
    #[should_panic]
    fn unmarshall_str_length_out_of_scope () {
      // "aaa"
      let strs: &[u8; 4] = &[97,97,97,0];
      let _unms_a = <&str>::from_gvariant (strs, 7);
    }

    #[test]
    #[should_panic]
    fn unmarshall_str_no_null () {
      let strs: &[u8; 3] = &[97,97,97];
      let _unms_a = <&str>::from_gvariant (strs, 0);
    }

    /* AST */
    #[test]
    fn ast_test () {
      let vt = GVariant::new("a({ss}abbs)", &[]);
      let astroot = vt.astroot.unwrap();
      assert_eq!(astroot.vtype, VariantType::Array);
      assert_eq!(astroot.children.len(), 1);

      let tuple = &astroot.children[0];
      assert_eq!(tuple.vtype, VariantType::Tuple);
      assert_eq!(tuple.children.len(), 4);

      let dict = &tuple.children[0];
      let ab = &tuple.children[1];
      let b = &tuple.children[2];
      let s = &tuple.children[3];

      assert_eq!(dict.vtype, VariantType::Dict);
      assert_eq!(dict.children.len(), 2);

      let da = &dict.children[0];
      let db = &dict.children[1];
      assert_eq!(da.vtype, VariantType::S);
      assert_eq!(db.vtype, VariantType::S);
      assert_eq!(da.children.len(), 0);
      assert_eq!(db.children.len(), 0);

      assert_eq!(ab.vtype, VariantType::Array);
      assert_eq!(ab.children.len(), 1);

      let ab_b = &ab.children[0];
      assert_eq!(ab_b.vtype, VariantType::B);
      assert_eq!(ab_b.children.len(), 0);

      assert_eq!(b.vtype, VariantType::B);
      assert_eq!(b.children.len(), 0);

      assert_eq!(s.vtype, VariantType::S);
      assert_eq!(s.children.len(), 0);
    }

    /* Get values */

    #[test]
    fn bool_get () {
      let gv_a = GVariant::new ("b", &[0x0]);
      let gv_b = GVariant::new ("b", &[0x1]);

      let val_f: bool = gv_a.get();
      let val_t: bool = gv_b.get();

      assert_eq!(val_f, false);
      assert_eq!(val_t, true);
    }

    #[test]
    fn f64_get () {
      let buffer = unsafe { transmute::<f64, [u8; 8]>(-1.24) };
      let gv = GVariant::new ("d", &buffer);

      let val: f64 = gv.get();

      assert_eq!(val, -1.24);
    }

    #[test]
    fn str_get () {
      let gv_s = GVariant::new ("s", &[0x61, 0x61, 0x61, 0x61, 0x0]); // "aaaa"
      let gv_o = GVariant::new ("o", &[0x62, 0x62, 0x62, 0x62, 0x0]); // "bbbb"
      let gv_g = GVariant::new ("g", &[0x63, 0x63, 0x63, 0x63, 0x0]); // "cccc"

      let val_s: &str = gv_s.get();
      let val_o: &str = gv_o.get();
      let val_g: &str = gv_g.get();

      assert_eq!(val_s, "aaaa");
      assert_eq!(val_o, "bbbb");
      assert_eq!(val_g, "cccc");
    }

    macro_rules! getter_test {
      ($id:ident, $t:ty, $size:expr, $f:expr) => {
        #[test]
        fn $id () {
          let buffer_a = unsafe { transmute::<$t, [u8; $size]>(<$t>::max_value()) };
          let buffer_b = unsafe { transmute::<$t, [u8; $size]>(<$t>::min_value()) };
          let gv_a = GVariant::new ($f, &buffer_a);
          let gv_b = GVariant::new ($f, &buffer_b);

          let val_a: $t = gv_a.get();
          let val_b: $t = gv_b.get();

          assert_eq!(val_a, <$t>::max_value());
          assert_eq!(val_b, <$t>::min_value());
        }
      }
    }

    getter_test!(u8_get, u8, 1, "y");
    getter_test!(i16_get, i16, 2, "n");
    getter_test!(u16_get, u16, 2, "q");
    getter_test!(i32_get, i32, 4, "i");
    getter_test!(i32_handle_get, i32, 4, "h");
    getter_test!(i64_get, i64, 8, "x");
    getter_test!(u64_get, u64, 8, "t");

    #[test]
    fn gvariant_get() {
      let gv = GVariant::new ("v", &[0x61, 0x0, 0x0, 0x73]);
      let vv: GVariant = gv.get();

      let s: &str = vv.get();

      assert_eq!("a", s);
    }

    #[test]
    fn maybe_get() {
      let mv = GVariant::new("mi", &[]);
      let foo: Option<i32> = mv.get ();
      assert_eq!(foo, None);

      let mv = GVariant::new("my", &[0x3]);
      let foo: Option<u8> = mv.get ();
      assert_eq!(foo, Some(3));

      let mv = GVariant::new("may", &[]);
      let foo: Option<GVariant> = mv.get ();
      match foo {
        None => { ; }
        _ => { panic!("A value was given when it shouldn't"); }
      }
    }

    #[test]
    #[should_panic]
    fn get_wrong_type () {
      let gv = GVariant::new ("as", &[]);
      let _val: u8 = gv.get ();

      let mgv = GVariant::new ("ms", &[]);
      let _val2: Option<u8> = mgv.get();
    }

    /* VariantNode tests */
    #[test]
    fn variant_type_is_fixed_size () {
      for s in vec!["i", "(iii)", "(ii(iii)i)"] {
        assert!(GVariant::new (s, &[]).astroot.unwrap().is_fixed_size());
      }
      for s in vec!["s", "ai", "{ss}", "ms", "(ivi)", "(viv)", "(i(i(s)))"] {
        assert!(!GVariant::new (s, &[]).astroot.unwrap().is_fixed_size());
      }
    }

    #[test]
    fn variant_type_is_container () {
      for s in vec!["as", "{ss}", "(iii)", "mi"] {
        assert!(GVariant::new (s, &[]).astroot.unwrap().is_container ());
      }
      for s in vec!["i", "s"] {
        assert!(!GVariant::new (s, &[]).astroot.unwrap().is_container ());
      }
    }

  #[test]
  fn variant_min_size () {
    for &(f,v) in [("(iaiimii)", 3*4),
                   ("()", 0), ("ai", 0), ("mi", 0), ("(ss)", 0),
                   ("(ynix)", 1+2+4+8),
                   ("b", 1), ("y", 1), ("n", 2), ("q", 2), ("i",4), ("u",4), ("h",4),
                   ("x",8), ("t",8), ("d", 8)].iter() {
      assert_eq!(VariantNode::from_format(&String::from(f)).unwrap().get_min_size(), v);
    }
  }

  /* Arrays */
  #[test]
  fn array_of_strings () {
    let a = GVariant::new ("as", &[0x61, 0x61, 0x61, 0x61, 0x0, 0x5]); // "aaaa" + 0x5 offset
    assert_eq!("aaaa", GetChildren::<&str>::get_child(&a, 0));

    let b = GVariant::new ("as", &[0x61,0x0,0x62,0x0,0x2,0x4]); // "a" "b" + 0x2 0x4 offsets
    assert_eq!("a", GetChildren::<&str>::get_child(&b, 0));
    assert_eq!("b", GetChildren::<&str>::get_child(&b, 1));

    /* u16 offsets */
    let mut vec_16: Vec<u8> = vec![0x61; 300];
    for i in [0x0 as u8, 0x62, 0x62, 0x62, 0x62, 0x0, 0x2d, 0x1, 0x32, 0x1].iter() { // "a" * 300, "bbbb", 0x12d (301), 0x132 (306)
      vec_16.push(*i);
    }

    unsafe {
      let first_string = std::str::from_utf8_unchecked(&vec_16[0..300]);
      let c = GVariant::new ("as", vec_16.as_ref());
      assert_eq!(first_string, GetChildren::<&str>::get_child(&c, 0));
      assert_eq!("bbbb", GetChildren::<&str>::get_child(&c, 1));
    }

    /* u32 offsets */
    let mut vec_32: Vec<u8> = vec![0x61; 70000];
    for i in [0x0 as u8, 0x62, 0x62, 0x62, 0x62, 0x0, 0x71, 0x11, 0x01, 0x00, 0x76, 0x11, 0x01, 0x00].iter() { // "a" * 70000, "bbbb", 0x11171 (70001), 0x11171 (70006)
      vec_32.push(*i);
    }

    unsafe {
      let first_string = std::str::from_utf8_unchecked(&vec_32[0..70000]);
      let d = GVariant::new ("as", vec_32.as_ref());
      assert_eq!(first_string, GetChildren::<&str>::get_child(&d, 0));
      assert_eq!("bbbb", GetChildren::<&str>::get_child(&d, 1));
    }

    // NOTE: The 64 bit version of this test takes just too long to run, we're assuming that it works
  }

  macro_rules! array_of_type_test {
    ($id:ident, $t:ty, $fmt:expr, $v:expr, $first:expr, $last:expr, $last_index:expr) => {
      #[test]
      fn $id () {
        let a = GVariant::new ($fmt, $v); // "aaaa" + 0x5 offset
        assert_eq!($first as $t, a.get_child(0));
        assert_eq!($last as $t, a.get_child($last_index));
      }
    }
  }

  array_of_type_test!(array_of_u8, u8, "ay",  &[0x61, 0x62, 0x63, 0x64, 0x65], 0x61, 0x65, 4);
  array_of_type_test!(array_of_u16, u16, "aq", &[0x61, 0x62, 0x64, 0x65], 0x6261, 0x6564, 1);
  array_of_type_test!(array_of_i16, i16, "an", &[0x61, 0x62, 0x64, 0x65], 0x6261, 0x6564, 1);
  array_of_type_test!(array_of_u32, u32, "au", &[0x61, 0x62, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69], 0x65646261, 0x69686766, 1);
  array_of_type_test!(array_of_i32, i32, "ai", &[0x61, 0x62, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69], 0x65646261, 0x69686766, 1);
  array_of_type_test!(array_of_i64, i64, "ax", &[0x61, 0x62, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69,
                                                 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78], 0x6968676665646261, 0x7877767574737271, 1);
  array_of_type_test!(array_of_u64, u64, "at", &[0x61, 0x62, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69,
                                                 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78], 0x6968676665646261, 0x7877767574737271, 1);
  array_of_type_test!(array_of_f64, f64, "ad", &[0x48, 0xe1, 0x7a, 0x14, 0xae, 0x47, 0xf5, 0x3f,
                                                 0x48, 0xe1, 0x7a, 0x14, 0xae, 0x47, 0xf5, 0xbf], 1.33, -1.33, 1);

  #[test]
  fn array_of_gvariant () {
    let a = GVariant::new("ay", &[0x61, 0x0, 0x0, 0x73, 0x62, 0x0, 0x0, 0x73, 0x04, 0x08]);
    let a1: GVariant = a.get_child(0);
    let a2: GVariant = a.get_child(9);

    assert_eq!("y", a1.format);
    assert_eq!(0x61 as u8, a1.get());

    assert_eq!("y", a2.format);
    assert_eq!(0x8 as u8, a2.get());

    let b = GVariant::new("av", &[0x61, 0x0, 0x0, 0x73, 0x62, 0x0, 0x0, 0x73, 0x04, 0x08]);
    let b1: GVariant = b.get_child(0);
    let b2: GVariant = b.get_child(1);

    assert_eq!("s", b1.format);
    assert_eq!("s", b2.format);

    let b = GVariant::new("may", &[0x1, 0x0]);
    let b1: GVariant = b.get_child(0);
    let b2: u8 = b1.get_child(0);

    assert_eq!("ay", b1.format);
    assert_eq!(1 as u8, b2);

    let b = GVariant::new("mas", &[0x61, 0x0, 0x2, 0x0]);
    let b1: GVariant = b.get_child(0);
    let b2: &str = b1.get_child(0);

    assert_eq!("as", b1.format);
    assert_eq!("a", b2);

    let b = GVariant::new("ms", &[0x0, 0x0]);
    let b1: GVariant = b.get_child(0);

    assert_eq!("s", b1.format);
    assert_eq!("", GetValue::<&str>::get(&b1));
  }

  #[test]
  #[should_panic]
  fn maybe_of_empty_array () {
    let b = GVariant::new("may", &[0x0]);
    let b1: GVariant = b.get_child(0);
    let _b2: u8 = b1.get_child(0);

    assert_eq!("ay", b1.format);
  }

  #[test]
  fn tuple_dict () {
    let b = GVariant::new("(yyy)", &[1u8, 2u8, 3u8]);
    let (b1, b2, b3): (u8,u8,u8) = (b.get_child(0), b.get_child(1), b.get_child(2));
    assert_eq!(b1, 1); assert_eq!(b2, 2); assert_eq!(b3, 3);
    println!("1");

    let b = GVariant::new("(ayay)", &[0x0]);
    let (b1, b2): (GVariant, GVariant) = (b.get_child(0),b.get_child(1));
    assert_eq!(b1.buffer.len(), 0);
    assert_eq!(b2.buffer.len(), 0);
    println!("2");

    let b = GVariant::new("(aiaiy)", &[0x01,0x00,0x00,0x00,0x0f,0x04,0x00]);
    let (b1, b2, b3): (GVariant, GVariant, u8) = (b.get_child(0),b.get_child(1),b.get_child(2));
    assert_eq!(b1.buffer.len(), 0);
    assert_eq!(b2.buffer.len(), 4);
    assert_eq!(b3, 0xf);

  }
}
